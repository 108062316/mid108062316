var user_room  = "lobby";
var user_uid = "";
var str_before_my_username_fir = "<div class='my-3 p-3 bg-white rounded box-shadow' style='text-align:right'><h6 class='border-bottom border-gray pb-2 mb-0'>";
var str_before_my_username_sec = "</h6><div class='media text-muted pt-3'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'><strong class='d-block text-gray-dark'>";
var str_before_other_username_fir = "<div class='my-3 p-3 bg-white rounded box-shadow'><h6 class='border-bottom border-gray pb-2 mb-0'>";
var str_before_other_username_sec = "</h6><div class='media text-muted pt-3'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'><strong class='d-block text-gray-dark'>";
var str_after_content = "</p></div></div>\n";
var str_before_room = "<span class='dropdown-item' id='";
var str_after_room =  "'onclick='roomchange(this.id)'>";

function init() {
    /*if (!('Notification' in window)) {
        console.log('This browser does not support notification');
    } else{
        console.log('This browser support notification');
    }*/
    var total_room = [];
    var first_count_room = 0;
    var second_count_room = 0;

    if (Notification.permission === 'default' || Notification.permission === 'undefined') {
        Notification.requestPermission(function(permission) {
          if (permission === 'granted') {// 使用者同意授權
            var notifyConfig = {
                body: '\\ ^o^ /', // 設定內容
                tag:'welcome'
              };
            var notification = new Notification('welcome to chatroom!', notifyConfig); // 建立通知
          }
        });
      }

    var user_email = '';
    firebase.auth().onAuthStateChanged(function(user) {
        var menu = document.getElementById('dynamic-menu');
        var menu2 = document.getElementById('dynamic-menu2');
        var create = document.getElementById('create');
        // Check user login
        if (user) {
            user_email = user.email;
            path_user_email = user_email.replaceAll(".",",");
            user_uid = user.uid;
            var userPath = "user/" + path_user_email;
            console.log("userPath:",userPath);
            var roomsRef = firebase.database().ref(userPath);
            roomsRef.once('value')
                .then(function(snapshot) {
                    console.log("in this");
                    document.getElementById('dynamic-menu2').innerHTML = total_room.join('');
                    var db = firebase.database();
                    db.ref(userPath).once("value",(snapshot)=>{
                        var data = snapshot.val();
                        //console.log("data",data);
                        for(i in data){
                            //console.log("in draw chatroom",i);
                            total_room[first_count_room++] = str_before_room + data[i].room + str_after_room + data[i].room + "</span>";
                        }
                        console.log(total_room[0])
                        document.getElementById('dynamic-menu2').innerHTML = total_room.join('');
                    });
                    roomsRef.once('value')
                        .then(function(snapshot) {
                            document.getElementById('dynamic-menu2').innerHTML = total_room.join('');
                            roomsRef.on('child_added', function(data) {
                            //console.log("sec:",second_count_room);
                            second_count_room += 1;
                            console.log("sec:",second_count_room);
                            if (second_count_room > first_count_room) {
                                var childData = data.val();
                                //total_post[total_post.length] = str_before_username + childData.email + "</strong>" + childData.data + str_after_content
                                total_room[total_room.length] = str_before_room + childData.room + str_after_room + childData.room + "</span>";
                                document.getElementById('dynamic-menu2').innerHTML = total_room.join('');
                            }
                        });
                    })
                })
                .catch(e => console.log(e.message));
            menu.innerHTML = "<span class='dropdown-item'>" + user.email + "</span><span class='dropdown-item' id='logout-btn'>Logout</span>";
            menu2.innerHTML = "<span class='dropdown-item' id='lobby' onclick='roomchange(this.id)'>" + "lobby" + "</span><span class='dropdown-item' id='room1' onclick='roomchange(this.id)'>room1</span>";
            
            //logout
            var logout_btn = document.getElementById("logout-btn")
            logout_btn.addEventListener('click',e=>{
                firebase.auth().signOut().then(function() {
                    alert('bye');
                }).catch((error)=>{
                    alert('logout fail');
                })
            })
            //create room
            var create_btn = document.getElementById("create");
            create_btn.addEventListener('click',e=>{
                var roomname = prompt("please enter your new room name");
                if(roomname == "lobby"){
                    alert("room name is existed")
                } else {
                    var checkUserPath = "user/" + path_user_email;
                    var updateRoomsRef = firebase.database().ref(checkUserPath);
                    var checkRoomRef = firebase.database().ref("room_list");
                    var check = true;
                    checkRoomRef.once('value').then(function(snapshot){
                        console.log("in check");
                        var data = snapshot.val();
                        for (i in data){
                            console.log(data[i].room," ",roomname);
                            if(data[i].room === roomname){
                                check = false;
                                console.log("check:",check);
                                break;
                            }
                        }
                    }).then(function(){
                        if(check == true && roomname!=null){
                            updateRoomsRef.push({
                                room: roomname
                            }).then(function(){
                                checkRoomRef.push({
                                    room:roomname
                                }).then(function(){
                                    alert("create success");
                                })
                            })
                        } else { 
                            if(roomname!=null)
                            alert("room name is existed")
                        }
                    })
                }
                //console.log(roomname);
                //check if roomname exist
                /*var checkUserPath = "user/" + path_user_email;
                var checkRoomsRef = firebase.database().ref(checkUserPath);
                var check = true;
                checkRoomsRef.once('value').then(function(snapshot){
                    console.log("in check");
                    var data = snapshot.val();
                    for (i in data){
                        console.log(data[i].room," ",roomname);
                        if(data[i].room === roomname){
                            check = false;
                            console.log("check:",check);
                            break;
                        }
                    }
                }).then(function(){
                    if(check == true && roomname!=null){
                        checkRoomsRef.push({
                            room: roomname
                        }).then(function(){
                            alert("push is success");
                        })
                    } else {
                        if(roomname!=null)
                        alert("room name is existed")
                    }
                })*/
            })

            var invite_btn = document.getElementById("invite");
            invite_btn.addEventListener('click',e=>{
                var inviteUserName = prompt("please enter the username");
                firebase.auth().fetchSignInMethodsForEmail(inviteUserName)
                .then(providers => {
                  if (providers.length === 0) {
                      alert("user not exist");
                  } else {
                        console.log("exist");
                        if(inviteUserName === user.email){
                            alert("you can't invite yorself")
                        } else if(user_room == "lobby"){
                            alert("Everyone are here!")
                        } else{
                            var inviteUser = inviteUserName.replaceAll(".",",");
                            var invitePath = "user/" + inviteUser;
                            var invitedb = firebase.database().ref(invitePath);
                            var check = true;
                            invitedb.once('value').then(function(snapshot){
                                console.log("in invite check");
                                var data = snapshot.val();
                                for (i in data){
                                    console.log(data[i].room," ",user_room);
                                    if(data[i].room === user_room){
                                        check = false;
                                        console.log("check:",check);
                                        break;
                                    }
                                }
                            }).then(function(){
                                if(check == true){
                                    invitedb.push({
                                        room: user_room
                                    }).then(function(){
                                        alert("invite success");
                                    })
                                } else {
                                    alert("user is in this room");
                                }
                            })

                        }
                  }
                }).catch(function(e){
                    alert(e.message)
                });
            })
  
        } else {
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            menu2.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            create.innerHTML = "";
            document.getElementById('post_list').innerHTML = "";
            document.getElementById('invite').innerHTML="";
            var now_room = document.getElementById("now_room");
            now_room.innerHTML = "<small id='now_room'>" + "please login" + "</small>";
        }
    });

    post_btn = document.getElementById('post_btn');
    post_txt = document.getElementById('comment');

    post_btn.addEventListener('click', function() {
        console.log(user_email);
        if (post_txt.value != "") {
           post_txt.value = post_txt.value.replace(/</g, "&lt;");
           post_txt.value = post_txt.value.replace(/>/g, "&gt;");
            var submitStamp = new Date().toLocaleTimeString();
            console.log(submitStamp);
            firebase.auth().onAuthStateChanged(function(user){
                if(user){
                    var db = firebase.database();
                    db.ref(user_room).push({
                        email: user.email,
                        data: post_txt.value,
                        subtime: submitStamp
                    }).then(function(){
                        post_txt.value = ""
                    })
                } else{
                    //alert("please login")
                }
            })
        } else {
            alert("you message is empty")
        }
    });


    // The html code for post
    //var str_before_username = "<div class='my-3 p-3 bg-white rounded box-shadow' style='text-align:right'><h6 class='border-bottom border-gray pb-2 mb-0'>Recent updates</h6><div class='media text-muted pt-3'><img src='img/test.svg' alt='' class='mr-2 rounded' style='height:32px; width:32px;'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'><strong class='d-block text-gray-dark'>";
    //var str_before_my_username = "<div class='my-3 p-3 bg-white rounded box-shadow' style='text-align:right'><h6 class='border-bottom border-gray pb-2 mb-0'>Recent updates</h6><div class='media text-muted pt-3'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'><strong class='d-block text-gray-dark'>";
    //var str_before_other_username = "<div class='my-3 p-3 bg-white rounded box-shadow'><h6 class='border-bottom border-gray pb-2 mb-0'>Recent updates</h6><div class='media text-muted pt-3'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'><strong class='d-block text-gray-dark'>";
    var postsRef = firebase.database().ref(user_room);
    var total_post = [];// List for store posts html
    var first_count = 0;// Counter for checking history post update complete
    var second_count = 0;// Counter for checking when to update new post
    
    postsRef.once('value')
        .then(function(snapshot) {

            /// Join all post in list to html in once
            now_user_email = firebase.auth().currentUser.email;
            //console.log("now use user is ",user_email);
            document.getElementById('post_list').innerHTML = total_post.join('');
            var db = firebase.database();
            db.ref(user_room).once("value",(snapshot)=>{
                console.log("in load chat2");
                var data = snapshot.val();
                for(i in data){
                    console.log("i:",data[i].email)
                    if(data[i].email == now_user_email){
                        total_post[first_count++]=str_before_my_username_fir + data[i].subtime + str_before_my_username_sec + data[i].email + "</strong>" + data[i].data + str_after_content
                    } else {
                        console.log("lalala");
                        total_post[first_count++]=str_before_other_username_fir + data[i].subtime + str_before_other_username_sec + data[i].email + "</strong>" + data[i].data + str_after_content
                    }
                    //total_post[first_count++]=str_before_username + data[i].email + "</strong>" + data[i].data + str_after_content
                }
                //console.log(total_post[0])
                document.getElementById('post_list').innerHTML = total_post.join('');
            });
            /// Add listener to update new post
            postsRef.on('child_added', function(data) {
                second_count += 1;
                if (second_count > first_count) {
                    var childData = data.val();
                    if(childData.email == now_user_email){
                        total_post[total_post.length]=str_before_my_username_fir + childData.subtime + str_before_my_username_sec + childData.email + "</strong>" + childData.data + str_after_content
                    } else {
                        total_post[total_post.length]=str_before_other_username_fir + childData.subtime + str_before_other_username_sec + childData.email + "</strong>" + childData.data + str_after_content
                        if (Notification.permission === 'granted') {
                            var notifyConfig2 = {
                                body: childData.data, // 設定內容
                                tag:'othertalking'
                              };
                              var myTitle = childData.email+" says:";
                              var notification = new Notification(myTitle, notifyConfig2); // 建立通知
                          }
                    }
                    //total_post[total_post.length] = str_before_username + childData.email + "</strong>" + childData.data + str_after_content
                    document.getElementById('post_list').innerHTML = total_post.join('');
                }
            });
        })
        .catch(e => console.log(e.message));
}

window.onload = function() {
    init();
};

function isHTML(str) {
    var doc = new DOMParser().parseFromString(str, "text/html");
    return Array.from(doc.body.childNodes).some(node => node.nodeType === 1);
  }

  function roomchange(NowRoom){
    console.log("in room: ")
    console.log(NowRoom);
    user_room = NowRoom;
    var postsRef = firebase.database().ref(NowRoom);
    var total_post = [];// List for store posts html
    var first_count = 0;// Counter for checking history post update complete
    var second_count = 0;// Counter for checking when to update new post
    var now_room = document.getElementById("now_room");
    now_room.innerHTML = "<small id='now_room'>" + user_room + "</small>";
    postsRef.once('value')
        .then(function(snapshot) {
            var now_user_email = firebase.auth().currentUser.email;
            console.log("in room change")
            console.log("now use user is ",now_user_email);
            document.getElementById('post_list').innerHTML = "";
            var db = firebase.database();
            db.ref(NowRoom).once("value",(snapshot)=>{
                console.log("in db find")
                var data = snapshot.val();
                for(i in data){
                    if(data[i].email == now_user_email){
                        total_post[first_count++]=str_before_my_username_fir + data[i].subtime + str_before_my_username_sec + data[i].email + "</strong>" + data[i].data + str_after_content
                    } else {
                        total_post[first_count++]=str_before_other_username_fir + data[i].subtime + str_before_other_username_sec + data[i].email + "</strong>" + data[i].data + str_after_content
                    }
                    //total_post[first_count++]=str_before_username + data[i].email + "</strong>" + data[i].data + str_after_content
                    console.log("fir:",first_count)
                }
                document.getElementById('post_list').innerHTML = total_post.join('');
            });
            /// Add listener to update new post
            postsRef.on('child_added', function(data) {
                second_count += 1;
                console.log("sec",second_count,"fir",first_count);
                if (second_count > first_count) {
                    console.log("append");
                    var childData = data.val();
                    if(childData.email == now_user_email){
                        total_post[total_post.length]=str_before_my_username_fir + childData.subtime + str_before_my_username_sec + childData.email + "</strong>" + childData.data + str_after_content
                    } else {
                        total_post[total_post.length]=str_before_other_username_fir + childData.subtime + str_before_other_username_sec + childData.email + "</strong>" + childData.data + str_after_content
                        if (Notification.permission === 'granted') {
                            console.log("in noti");
                            var notifyConfig2 = {
                                body: childData.data, // 設定內容
                                tag:'othertalking'
                              };
                              var myTitle = childData.email+" says:";
                              var notification = new Notification(myTitle, notifyConfig2); // 建立通知
                          }
                    }
                    //total_post[total_post.length] = str_before_username + childData.email + "</strong>" + childData.data + str_after_content
                    document.getElementById('post_list').innerHTML = total_post.join('');
                }
            });
        })
        .catch(e => console.log(e.message));
  }
