function initApp() {
    // Login with Email/Password
    var txtEmail = document.getElementById('inputEmail');
    var txtPassword = document.getElementById('inputPassword');
    var btnLogin = document.getElementById('btnLogin');
    var btnGoogle = document.getElementById('btngoogle');
    var btnSignUp = document.getElementById('btnSignUp');

    btnLogin.addEventListener('click', function() {
        /// TODO 2: Add email login button event
        ///         1. Get user input email and password to login
        ///         2. Back to index.html when login success
        ///         3. Show error message by "create_alert()" and clean input field
        console.log("hello");
        var email = document.getElementById("inputEmail")
        var password = document.getElementById("inputPassword")

        console.log(email.value,password.value)
        firebase.auth().signInWithEmailAndPassword(email.value, password.value).then(function(){
            window.location.href = "index.html"
        }).catch(function(error) {
            // Handle Errors here.
            var errorCode = error.code;
            var errorMessage = error.message;
            create_alert("error",errorMessage);
        });
    });

    btnGoogle.addEventListener('click', function() {
        /// TODO 3: Add google login button event
        ///         1. Use popup function to login google
        ///         2. Back to index.html when login success
        ///         3. Show error message by "create_alert()"
        var provider = new firebase.auth.GoogleAuthProvider();
        firebase.auth().signInWithPopup(provider).then(function(result) {
            // This gives you a Google Access Token. You can use it to access the Google API.
            //window.location.href = "index.html"
            var token = result.credential.accessToken;
            // The signed-in user info.
            var user = result.user;
            var newUserEmail = user.email.replaceAll(".",",")
            var userPath = "user/" + newUserEmail;
            var db = firebase.database();
            //console.log("in google sign in:",userPath);

            db.ref(userPath).push({
                room: "lobby"
            }).then(function(){
                create_alert("success","SignUpSuccess");
                window.location.href = "index.html"
            })
            // ...
          }).catch(function(error) {
            // Handle Errors here.
            var errorCode = error.code;
            var errorMessage = error.message;
            // The email of the user's account used.
            var email = error.email;
            // The firebase.auth.AuthCredential type that was used.
            var credential = error.credential;
            create_alert("error",errorMessage);
          });



    });

    btnSignUp.addEventListener('click', function() {
        /// TODO 4: Add signup button event
        ///         1. Get user input email and password to signup
        ///         2. Show success message by "create_alert" and clean input field
        ///         3. Show error message by "create_alert" and clean input field
        //console.log("click");
        var email = document.getElementById("inputEmail")
        var password = document.getElementById("inputPassword")
        firebase.auth().createUserWithEmailAndPassword(email.value,password.value).then((res)=>{
            console.log(res.user.uid,"now test");
            var newUserEmail = res.user.email;
            newUserEmail = newUserEmail.replaceAll(".",",");
            var userPath = "user/" + newUserEmail;
            var db = firebase.database();
            db.ref(userPath).push({
                room: "lobby"
            }).then(function(){
                create_alert("success","SignUpSuccess");
                email.value = "";
                password.value = "";
            })
        }).catch(function(error) {
            // Handle Errors here.
            var errorCode = error.code;
            var errorMessage = error.message;
            create_alert("error",errorMessage);
            email.value = "";
            password.value = "";
        });
        //create_alert("success","SignUpSuccess");
        //var db = firebase.database();
        //var userFolder = email.value.replaceAll(".",",");
        /*db.ref().push({
            email: user.email,
            data: post_txt.value,
            subtime: submitStamp
        }).then(function(){
            post_txt.value = ""
        })
        email.value = "";
        password.value = "";*/


    });
}

// Custom alert
function create_alert(type, message) {
    var alertarea = document.getElementById('custom-alert');
    if (type == "success") {
        str_html = "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong>Success! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    } else if (type == "error") {
        str_html = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Error! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    }
}

window.onload = function() {
    initApp();
};

function init( ) {
    _oTag = document.getElementById("preloader");
    realWeb = document.getElementById("afterload");
    realWeb.style.display = "none";
    setTimeout(function(){
        console.log("in set");
        _oTag.style.display = "none";  //隱藏動畫
        realWeb.style.display = "initial";
    },2000);
  }
  
if (window.attachEvent) {
    window.addEventListener('onload', init);
  } else {
    window.addEventListener('load', init, false);
  }
  