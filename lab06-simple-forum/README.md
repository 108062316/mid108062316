# Software Studio 2021 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* Project Name : Midterm_Project_108062316

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|15%|Y|
|Firebase Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|20%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Y|

# 作品網址：https://mid108062316.web.app

## Website Detail Description
一個聊天室，可以用gmail登入或以電子郵件註冊。  
登入後點擊Account查看帳戶資訊  
點擊Chatroom查看該帳號在那些聊天室中  
點擊Chatroom的名字可以跳轉到該Chatroom  
點擊create可以創建新的聊天室，不能創建重名的聊天室。  
點擊invite可以邀請他人進到當前所在的聊天室，不能將其他人邀請至lobby
，不能重複邀請同一個人。  
當所在的聊天室有其他人傳送訊息時會有通知，firefox會顯示在瀏覽器，google會顯示在電腦右下角的通知處。


# Components Description : 
1. Membership Mechanism : 可以做登入和註冊
2. Firebase Page : 如題所示
3. Database : 可以依權限對資料庫做讀/寫
4. RWD : 如題所示
5. Topic Key Function : 可以創建聊天室、邀請他人、傳送訊息與讀取訊息。
6. Third-Party Sign In : 可以用google登入
7. Chrome Notification : 當所在的聊天室有其他人傳送訊息時會有通知，firefox會顯示在瀏覽器，google會顯示在電腦右下角的通知處。
8. Use CSS Animation : 聊天室跳轉至登入畫面時會有動畫
9. Security Report : 使用者傳送html code不會使聊天室爛掉


# Other Functions Description : 
1. 聊天室資訊顯示優化 : 可以看到當前所在聊天室與每則訊息的傳送時間，自己的訊息會顯示在右邊


